#!/usr/bin/env python

from tkinter import filedialog
from tkinter import messagebox
from tkinter import colorchooser
from tkinter import *
from functools import partial
from PIL import ImageTk, Image
import random
import math
import os

class txtImg:
    def __init__(self, file_to_process: str):
        self.file_to_process = file_to_process
        self.img_object = None
        self.pixel_list = []
        self.letter_dict = {}
        self.is_error = False
        self.str_error = ''
        self.width = 0
        self.height = 0
        self.number_of_pixels = 0
        self.parse_text()
        self.generate_colors()

    def parse_text(self) -> bool:
        if os.access(self.file_to_process, os.R_OK):
            if os.path.isfile(self.file_to_process):
                with open(self.file_to_process, "r") as f:
                    while True:
                        char = None
                        try:
                            char = f.read(1)
                        except UnicodeDecodeError:
                            self.is_error = True
                            self.str_error += self.file_to_process + ' is not a RAW text'
                            return False
                        except IOError:
                            self.is_error = True
                            self.str_error += 'Cannot open: ' + self.file_to_process
                            return False
                        if char is not None:
                            # If not end of file
                            if char != '':
                                # Lowercase char
                                char = char.lower()
                                if char.isalpha() is False:
                                    continue
                                if char in self.letter_dict:
                                    self.letter_dict[char]['position'].append(self.number_of_pixels)
                                else:
                                    self.letter_dict.update({char:{'position':[self.number_of_pixels]}})
                                self.number_of_pixels += 1
                            # End of file
                            else:
                                return True
            else:
                self.is_error = True
                self.str_error += self.file_to_process + ' is a directory'
        else:
            self.is_error = True
            self.str_error += 'You don\'t have permission to read: ' + self.file_to_process

    def generate_colors(self):
        for letter in self.letter_dict:
            r = random.randint(0,255)
            g = random.randint(0,255)
            b = random.randint(0,255)
            rgb = (r,g,b)
            self.letter_dict[letter].update({'color': rgb})

    def set_a_letter_color(self, letter: str, color: list):
        self.letter_dict[letter]['color'] = color

    def update_pixel_list(self):
        # Generate a sized list full of black pixels
        for _ in range(self.number_of_pixels):
            self.pixel_list.append((0,0,0))
        # Fill the pixel list
        for letter in self.letter_dict:
            for position in self.letter_dict[letter]['position']:
                self.pixel_list[position] = self.letter_dict[letter]['color']

    def generate_image(self):
        # If not a 1:1 ratio image
        # We increase width
        self.height = self.width = int(math.sqrt(self.number_of_pixels))
        diff = self.number_of_pixels - self.width**2
        add_column = int(diff/self.width)
        self.width += add_column
        self.img_object = Image.new('RGB', (self.width, self.height))
        y = 0
        i = 0
        # If we got extra pixels we dont draw them
        max_index = self.width*self.height
        while i < max_index:
            for x in range(self.width):
                self.img_object.putpixel((x,y), self.pixel_list[i])
                i += 1
            y += 1

    def save_image(self, file :str):
        self.img_object.save(file , "PNG")

class GUI:
    def __init__(self):
        self.opened_file = False
        self.color_picker_draw = False
        self.color_picker = None
        self.zoom = 1
        self.home = os.path.expanduser("~")
        # Main Window
        self.user_window = Tk()
        self.user_window.read_file = None
        self.user_window.title('Text 2 Image')
        self.user_window.rowconfigure(1, weight=1)
        self.user_window.columnconfigure(0, weight=1)
        # Button Frame
        self.button_frame = Frame(self.user_window)
        self.button_frame.grid(row=0,column=0)
        self.button_frame.rowconfigure(0, weight=1)
        self.button_frame.columnconfigure(7, weight=1)
        self.open_button = Button(self.button_frame, text = "Open", command = self.ask_open)
        self.open_button.grid(row=0, column=0)
        self.save_button = Button(self.button_frame, text = "Save", command = self.ask_save)
        self.save_button.grid(row=0, column=1)
        self.reprocess_button = Button(self.button_frame, text = "Reprocess", command = self.ask_reprocess)
        self.reprocess_button.grid(row=0, column=2)
        self.color_button = Button(self.button_frame, text = "Color Picker", command = self.show_color_grid)
        self.color_button.grid(row=0, column=3)
        self.zoom_in_button = Button(self.button_frame, text = "Zoom -", command = self.zoom_out)
        self.zoom_in_button.grid(row=0, column=4)
        self.zoom_init_button = Button(self.button_frame, text = "No Zoom", command = self.zoom_init)
        self.zoom_init_button.grid(row=0, column=5)
        self.zoom_out_button = Button(self.button_frame, text = "Zoom +", command = self.zoom_in)
        self.zoom_out_button.grid(row=0, column=6)
        self.quit_button = Button(self.button_frame, text = "Quit", command = self.ask_quit)
        self.quit_button.grid(row=0, column=7)
        # Image Frame
        self.image_frame = Frame(self.user_window)
        self.image_frame.grid(row=1,column=0)
        self.label_image = Label(self.image_frame)
        self.label_image.grid(row=0, column=1)
        self.user_window.mainloop()

    def ask_open(self):
        if self.color_picker_draw is True:
            self.color_picker.destroy()
        if self.user_window.read_file:
            old_file = self.user_window.read_file
        self.user_window.read_file =  filedialog.askopenfilename(initialdir = self.home, title = "Choose a RAW text file",filetypes = (("txt files","*.txt"),("all files","*")))
        if  self.user_window.read_file:
            self.zoom = 1
            self.img = txtImg(self.user_window.read_file)
            self.opened_file = True
            self.show_image()
        else:
            if old_file:
                self.user_window.read_file = old_file

    def ask_save(self):
        if self.opened_file is True:
            self.user_window.write_file = filedialog.asksaveasfilename(initialdir = self.home,title = "Save image to:",filetypes = (("PNG file","*.png"),("all files","*.*")))
            self.img.save_image(self.user_window.write_file)
        else:
            messagebox.showerror("Error", "You need to open a file")

    def ask_reprocess(self):
        if self.opened_file is True:
            self.img = txtImg(self.user_window.read_file)
            self.show_image()
            if self.color_picker_draw is True:
                self.show_color_grid()
        else:
            messagebox.showerror("Error", "You need to open a file")

    def ask_quit(self):
        self.user_window.quit()
        exit()

    def show_color_grid(self):
        if self.opened_file is True:
            x = 0
            y = 0
            if self.color_picker_draw is True:
                self.color_picker.destroy()
            self.color_picker = Toplevel(self.user_window)
            for letter in self.img.letter_dict:
                hex_color = '#%02x%02x%02x' % self.img.letter_dict[letter]['color']
                nb_occurence = len(self.img.letter_dict[letter]['position'])
                color_frame = Frame(self.color_picker, bd=2,relief='ridge',width=10)
                color_frame.grid(row=y,column=x)
                color_tuple = (letter,hex_color)
                l = Button(color_frame, text=letter + '\n' + str(nb_occurence), command = partial(self.ask_color, color_tuple))
                l.grid(row=y,column=x)
                l.config(font=("Courier", 20))
                c = Button(color_frame, bg=hex_color, width=10, command = partial(self.ask_color, color_tuple))
                c.grid(row=y+1,column=x)
                self.user_window.update()
                x+=1
                if x >= 7:
                    y += 1
                    x = 0
            self.color_picker_draw = True
        else:
            messagebox.showerror("Error", "You need to open a file")

    def ask_color(self, color_tuple:tuple):
        color = colorchooser.askcolor(color_tuple[1])
        if color[0]:
            rgb = (int(color[0][0]),int(color[0][1]),int(color[0][2]))
            self.img.set_a_letter_color(color_tuple[0],rgb)
            self.show_color_grid()
            self.show_image()

    def show_image(self):
        if self.opened_file is True:
            self.img.update_pixel_list()
            self.img.generate_image()
            width = int(self.zoom * self.img.width)
            height = int(self.zoom * self.img.height)
            self.img.img_object = self.img.img_object.resize((width, height), Image.ANTIALIAS)
            self.image = ImageTk.PhotoImage(self.img.img_object)
            self.label_image.config(image=self.image)
            self.user_window.update()

    def zoom_in(self):
        self.zoom += 0.5
        self.show_image()

    def zoom_init(self):
        self.zoom = 1
        self.show_image()

    def zoom_out(self):
        self.zoom /= 2
        self.show_image()

def main():

    _gui = GUI()

if __name__ == "__main__":
    main()
