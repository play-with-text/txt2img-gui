#!/bin/sh

# Install dependency
pip install Pillow

# Make file executable
chmod +x txt2img.py

# Copy to user directory
sudo cp txt2img.py /usr/bin/txt2img-gui